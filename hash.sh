#!bin/bash

export HASH_NAME=$(docker run --rm -v "${PWD}":/workdir mikefarah/yq eval '.spec.template.spec.containers[].envFrom[].configMapRef.name' app.yaml)
export HASH_NAME+=`md5sum config.yaml | awk '{ print $1 }' | sed 's/\(.*\)/-\1 /'`
echo $HSAH_NAME
docker run --rm --env HASH_NAME=$HASH_NAME -v "${PWD}":/workdir mikefarah/yq eval '.metadata.name=env(HASH_NAME)' -i /workdir/config.yaml
